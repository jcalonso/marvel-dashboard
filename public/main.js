$( document ).ready( function () {

    $('.hash-popover' ).popover({trigger:'click'});

    if ( $( '.typeahead' ).length === 0 ) {
        return;
    }

    $( '.typeahead' ).tagsinput( {
                                     itemValue: 'id',
                                     itemText:  'name'
                                 } );

    $( '.typeahead' ).tagsinput( 'input' ).typeahead( {
                                                          valueKey:  'name',
                                                          prefetch:  '/characters/json',
                                                          freeInput: false,
                                                          limit:     10

                                                      } ).bind( 'typeahead:selected', $.proxy( function ( obj, datum ) {
        this.tagsinput( 'add', datum );
        this.tagsinput( 'input' ).typeahead( 'setQuery', '' );
    }, $( '.typeahead' ) ) );

    $( '.streams-create form' ).on( 'submit', function () {

        $( '#selectedCharacters' ).val( $( '.typeahead' ).val() );
    } );


} );

