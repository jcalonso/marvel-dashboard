# Marvel Dashboard -  Server Setup

## Pre-requsites

- Install VirtualBox ([Download](https://www.virtualbox.org/wiki/Downloads))
- Install Vagrant ([Download](http://downloads.vagrantup.com/))
- Install Ansible ([Download](http://www.ansibleworks.com/docs/gettingstarted.html))

## Installation

* Run `vagrant up`
* Add your api keys in /app/config/development
* Open 192.168.111.42 in your browser

## Tech stack

* Php-fpm
* Nginx
* Mongodb for storage
* Redis for queues
* Supervisor for queue process
* Crond for pulling new interactions data 

## Libraries

* Laravel framework
* datasift/datasift-php
* jenssegers/mongodb as laravel to mongodb driver
* caseysoftware/marvel-helper a Marvel api wrapper
* Bootstrap 3.0
* Twitter Typeahead
* Bootstrap-tagsinput