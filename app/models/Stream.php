<?php
use Jenssegers\Mongodb\Model as Eloquent;

class Stream extends Eloquent
{

    public function streamType()
    {

        return $this->hasMany( 'streamType' );
    }
}