<?php
use Jenssegers\Mongodb\Model as Eloquent;

class StreamType extends Eloquent
{
    public function stream()
    {
        return $this->belongsTo( 'stream' );
    }
}