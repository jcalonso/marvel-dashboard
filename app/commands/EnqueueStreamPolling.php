<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EnqueueStreamPolling extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:pull-interactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls interactions from a stream subscription';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info( 'Started enqueuing streams for polling' );

        $streams = Stream::where( 'subscriptionId' != '' )
                         ->where( 'status', 'active' )
                         ->get();

        if ( count( $streams ) == 0 ) {
            $this->info( 'No streams found' );

        }

        foreach ( $streams as $stream ) {

            Queue::push( 'PullStream', $stream->subscriptionId );

            $this->info( 'Enqueued stream for polling: ' . $stream->name );
        }

        $this->info( 'Finish enqueuing streams for polling' );

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
                array( 'subscriptionId', InputArgument::OPTIONAL, 'Specify the subscription id to pull.' ),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
                array( 'example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null ),
        );
    }

}
