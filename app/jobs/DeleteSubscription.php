<?php

class DeleteSubscription
{

    public function fire( $job, $streamId )
    {
        Log::info( 'Deleting stream from DataSift' );

        // Get the stream details from  storage
        $stream = Stream::find( $streamId );

        // Connect to datasift api
        $user = new DataSift_User( Config::get( 'datasift.username' ), Config::get( 'datasift.api_key' ) );

        try {
            $subscription = $user->getPushSubscription( $stream->subscriptionId );
            $subscription->delete();

        } catch ( Exception $e ) {

            Log::warning( 'The stream id was not found in DataSift servers' );
        }

        // Update the stream details in our internal storage
        $stream->status = 'stopped';
        $stream->subscriptionId = '';
        $stream->save();

        Log::info( 'Updated local data for current stream' );

        $job->delete();

        Log::info( 'Subscription deleted' );
    }

}