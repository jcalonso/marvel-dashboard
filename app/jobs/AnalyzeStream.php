<?php

class AnalyzeStream
{

    public function fire( $job, $streamHash )
    {
        Log::info( 'Starting interactions analyzer' );

        // Get the stream details from  storage
        $interactionsChunk = Interaction::where( 'hash', $streamHash )
                                        ->get();

        Log::info( 'Found ' . $interactionsChunk->count() . ' chunks of interactions' );

        // If there is nothing to analyze, quit
        if ( $interactionsChunk->count() === 0 ) {

            $job->delete();
            return;
        }

        $characters = [ ];
        $totalPopularity = 0;

        // Loop through every interaction
        // This needs tons of optimization,
        // Ideally lots of this logic can be done with a
        // mongodb query ... I need to read more about mongo
        foreach ( $interactionsChunk as $chunk ) {

            foreach ( $chunk->interactions as $interaction ) {

                if ( isset( $interaction['interaction']['tags'] ) ) {

                    foreach ( $interaction['interaction']['tags'] as $tag ) {

                        $characters[$tag]['popularity'] = isset( $characters[$tag]['popularity'] ) ? ( $characters[$tag]['popularity'] + 1 ) : 1;
                        $totalPopularity++;

                        try {
                            $characters[$tag]['sentiment'][] = $interaction['salience']['content']['sentiment'];
                        } catch ( Exception $e ) {
                            Log::info( 'No sentiment found' );
                        }

                        if ( isset( $interaction['demographic'] ) ) {

                            $gender = $interaction['demographic']['gender'];
                            Log::info( 'Gender:' . $gender );
                            $characters[$tag]['gender'][$gender] = isset( $characters[$tag]['gender'][$gender] ) ? ( $characters[$tag]['gender'][$gender] + 1 ) : 1;
                        } else {
                            Log::info( 'No demographic found: ' );
                        }
                    }
                }
            }


        }

        $results = [ ];
        foreach ( $characters as $id => $character ) {

            $characterDetail = Character::where( 'id', $id )->first();
            $avgSentiment = array_sum( $character['sentiment'] ) / count( $character['sentiment'] );

            $mostlyMale = ( isset( $character['gender']['mostly_male'] ) ) ? $character['gender']['mostly_male'] : 0;
            $mostlyFemale = ( isset( $character['gender']['mostly_female'] ) ) ? $character['gender']['mostly_female'] : 0;
            $male = ( isset( $character['gender']['male'] ) ) ? $character['gender']['male'] : 0;
            $female = ( isset( $character['gender']['female'] ) ) ? $character['gender']['female'] : 0;

            $results[$id] = [ 'name'       => $characterDetail->name,
                              'popularity' => ( ( $character['popularity'] * 100 ) / $totalPopularity ),
                              'sentiment'  => $avgSentiment,
                              'male'       => $mostlyMale + $male,
                              'female'     => $mostlyFemale + $female ];
        }

        $stream = Stream::where( 'hash', $streamHash )->first();
        $stream->results = $results;
        $stream->save();

        $job->delete();

        Log::info( 'Analysis finished:'.$streamHash );

    }

}