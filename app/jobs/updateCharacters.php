<?php

class UpdateCharacters
{

    public function fire( $job )
    {
        Log::info( 'Updating characters from Marvel API' );

        // Drop current documents
        DB::collection( 'character' )->truncate();

        $client = new Services_Marvel( Config::get( 'marvel.public_key' ), Config::get( 'marvel.private_key' ) );

        $offset = 1;

        while ( true ) {
            $characters = $client->characters->index( $offset, 100 );

            if ( count( $characters ) === 0 ) {

                Log::info( 'Finished updating characters from Marvel API' );

                $job->delete();
                break;
            }

            foreach ( $characters as $character ) {

                $characterModel = new Character();
                $characterModel->id = $character->id;
                $characterModel->name = $character->name;
                $characterModel->description = $character->description;
                $characterModel->modified = $character->modified;
                $characterModel->thumbnail = $character->thumbnail;
                $characterModel->resourceURI = $character->resourceURI;
                $characterModel->comics = $character->comics;
                $characterModel->series = $character->series;
                $characterModel->stories = $character->stories;
                $characterModel->events = $character->events;
                $characterModel->urls = $character->urls;
                $characterModel->series = $character->series;
                $characterModel->save();

                Log::info( 'Adding: ' . $character->name );

            }

            $offset = $offset + 1;
        }

    }

}