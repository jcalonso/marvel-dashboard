<?php

class CreateStream
{

    public function fire( $job, $streamId )
    {
        Log::info( 'Creating new stream in DataSift' );

        // Get the stream details from  storage
        $stream = Stream::find( $streamId );

        // Connect to datasift api
        $user = new DataSift_User( Config::get( 'datasift.username' ), Config::get( 'datasift.api_key' ) );
        $definition = new DataSift_Definition( $user, $stream->csdl );

        // Compile csdl query
        $definition->compile();

        Log::info( 'CSDL compiled' );

        // Subscribe to this stream
        $pushDefinition = $user->createPushDefinition();
        $pushDefinition->setOutputType( 'pull' );
        $pushDefinition->setOutputParam( 'format', 'json_meta' );
        $subscription = $pushDefinition->subscribeStreamHash( $definition->getHash(), $stream->name );

        Log::info( 'Subscribed to stream' );

        // Update the stream details in our internal storage
        $stream->dpu = $definition->getTotalDPU();
        $stream->hash = $definition->getHash();
        $stream->status = $subscription->getStatus();
        $stream->subscriptionId = $subscription->getId();
        $stream->save();

        Log::info( 'Updated local data for current stream' );

        $job->delete();

        Log::info( 'New stream created' );

    }

}