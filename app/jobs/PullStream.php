<?php

class PullStream
{

    /**
     * Not very nice code, but datasift-php doesn't have pull :(
     * todo: Make a PR with the pull functionality
     * @param $job
     * @param $subscriptionId
     */
    public function fire( $job, $subscriptionId )
    {
        Log::info( 'Pulling new stream data from DataSift' );

        $ch = curl_init();

        $httpHeaders = [ ];
        $httpHeaders[] = "Accept: application/json";
        $httpHeaders[] = 'Authorization: ' . Config::get( 'datasift.username' ) . ':' . Config::get( 'datasift.api_key' );

        $params = [ 'id' => $subscriptionId ];
        $url = "http://api.datasift.com/v1/pull";
        $url .= '?' . http_build_query( $params );

        curl_setopt( $ch, CURLOPT_HTTPHEADER, $httpHeaders );
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        $interactions = json_decode( curl_exec( $ch ) );
        curl_close( $ch );

        error_log( json_encode( $interactions ) );

        if ( isset( $interactions->error ) || empty( $interactions ) ) {

            Log::error( $interactions->error );
            $job->delete();
            return;
        }

        if ( $interactions->count > 0 ) {

            Log::info( 'Found ' . $interactions->count . ' new interactions' );

            $interaction = new Interaction();
            $interaction->subscriptionId = $interactions->id;
            $interaction->hash = $interactions->hash;
            $interaction->hash_type = $interactions->hash_type;
            $interaction->interactions = $interactions->interactions;
            $interaction->count = $interactions->count;
            $interaction->save();
        }

        // Update refresh date
        $stream = Stream::where( 'subscriptionId', $subscriptionId )->first();
        $stream->last_pull = date( 'Y-m-d H:i:s' );
        $stream->save();

        Queue::push( 'AnalyzeStream', $interactions->hash );

        $job->delete();

        Log::info( 'Finished pulling from stream' );
    }

}