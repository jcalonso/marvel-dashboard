<?php

class DataSiftTest extends TestCase
{

    /**
     * Intentionally create an exception to verify correct connection to DataSift Api
     * @expectedException DataSift_Exception_CompileFailed
     */
    public function testConnection()
    {

        // Create the user
        $user = new DataSift_User( Config::get( 'datasift.username' ), Config::get( 'datasift.api_key' ) );

        $definition = new DataSift_Definition( $user, 'interaction.content SomethingWrong "devops" ' );

        $definition->validate();
    }

}
 