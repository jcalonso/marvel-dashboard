<?php

class CharacterTest extends TestCase
{

    /**
     * Verifies the correct installation of mongodb, php driver for mongo, and laravel configuration
     */
    public function testAddCharacter()
    {
        $character = new Character();

        $character->name = 'Wolverine';

        $character->save();

        $this->assertRegExp("/^[0-9a-fA-F]{24}$/", $character->id );

    }
}
 