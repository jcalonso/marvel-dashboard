<?php

class MarvelApiTest extends TestCase
{
    const WOLVERINE_MARVEL_ID = 1009718;

    /**
     * A test to verify that we have loaded the marvel api client library and we can connect
     * to their service
     */
    public function testConnection()
    {

        $client = new Services_Marvel( Config::get( 'marvel.public_key' ), Config::get( 'marvel.private_key' ) );

        $characters = $client->characters->index( 1, 5, array( 'name' => 'Wolverine' ) );

        $this->assertEquals( SELF::WOLVERINE_MARVEL_ID, $characters[0]->id );
    }

}
