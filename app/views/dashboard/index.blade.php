@extends('layout')
@section('content')


<div class="container">

    <h2>Dashboard</h2>

    @foreach( $widgets as $widget)

    <div class="row">

        <div class="col-md-4">

            @include($widget['streamType']->widget_template)

        </div>
    </div>

    @endforeach


</div>
@stop