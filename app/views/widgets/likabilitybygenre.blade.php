<div class="block">
    <h3>{{ str_limit($widget['stream']->name, $limit = 25, $end = '...') }}</h3>

    @include('widgets.carousel', array('widget' => $widget))

    <p class="lead">{{ $widget['streamType']->name }}</p>

    @if( isset( $widget['stream']->results ) )
    @foreach($widget['stream']->results as $key => $result)
    <?php $genderTotal = $result['male'] + $result['female']; ?>
    <?php $genderTotal = ( $genderTotal === 0 ) ? 1 : $genderTotal; ?>
    <?php $malePercentage = ( $result['male'] * 100 ) / $genderTotal ?>
    <?php $femalePercentage = ( $result['female'] * 100 ) / $genderTotal ?>
    <span class="small">{{ $result['name'] }}</span>

    <div class="progress">
        <div class="progress-bar progress-bar-success" style="width: {{ $malePercentage  }}%">
            <span>{{ round($malePercentage,2)  }}% Male</span>
        </div>
        <div class="progress-bar progress-bar-warning" style="width: {{ $femalePercentage  }}%">
            <span>{{ round($femalePercentage,2)  }}% Female</span>
        </div>
    </div>
    @endforeach
    @endif

    <div class="text-right">
                <span class="label label-default">
                    @if( isset($widget['stream']->last_pull) )
                        Updated at: {{ $widget['stream']->last_pull }}
                    @else
                        Waiting for data ...
                    @endif
                </span>
    </div>