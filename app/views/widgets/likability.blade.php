<div class="block">
    <h3>{{ str_limit($widget['stream']->name, $limit = 25, $end = '...') }}</h3>

    @include('widgets.carousel', array('widget' => $widget))

    <p class="lead">{{ $widget['streamType']->name }}</p>
    <span class="small">0 is neutral</span>

    @if( isset( $widget['stream']->results ) )
    <ul class="list-group">
    @foreach($widget['stream']->results as $key => $result)
        <li class="list-group-item">
            @if( round($result['sentiment'], 2) > 0 )
                <?php $badgeColor = 'alert-success'; ?>
            @elseif( round($result['sentiment'], 2) == 0 )
                <?php $badgeColor = ''; ?>
            @elseif( round($result['sentiment'], 2) )
                <?php $badgeColor = 'alert-danger'; ?>
            @endif
            {{ $result['name'] }}  <span class="badge {{ $badgeColor }}"> {{ round($result['sentiment'], 2) }}</span>
        </li>
    @endforeach
    </ul>
    @endif

    <div class="text-right">
                <span class="label label-default">
                    @if( isset($widget['stream']->last_pull) )
                        Updated at: {{ $widget['stream']->last_pull }}
                    @else
                        Waiting for data ...
                    @endif
                </span>
    </div>