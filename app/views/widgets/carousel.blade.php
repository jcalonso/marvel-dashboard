<div id="carousel-widget-{{ $widget['stream']->id }}" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php $carouselCounter = 0; ?>
        @foreach( $widget['characters'] as $character )

        <li data-target="#carousel-widget-{{ $widget['stream']->id }}"
            data-slide-to="{{ $carouselCounter }}"
                <?= ( $carouselCounter === 0 ) ? 'class="active"' : '' ?>>
        </li>
        <?php $carouselCounter++; ?>
        @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <?php $carouselCounter = 0; ?>
    <div class="carousel-inner">

        @foreach( $widget['characters'] as $character )
        <div class="item <?= ( $carouselCounter === 0 ) ? "active" : '' ?>">
            <img class="thumbnail"
                 src="{{ $character->thumbnail['path'] }}.{{ $character->thumbnail['extension'] }}"/>

            <div class="carousel-caption">
                {{ $character->name }}
            </div>
        </div>
        <?php $carouselCounter++; ?>
        @endforeach
    </div>


    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-widget-{{ $widget['stream']->id }}" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-widget-{{ $widget['stream']->id }}" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>