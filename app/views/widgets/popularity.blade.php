<div class="block">
    <h3>{{ str_limit($widget['stream']->name, $limit = 25, $end = '...') }}</h3>

    @include('widgets.carousel', array('widget' => $widget))

    <p class="lead">{{ $widget['streamType']->name }}</p>

    @if( isset( $widget['stream']->results ) )
    @foreach($widget['stream']->results as $key => $result)
    <div class="progress progress-striped">
        <div class="progress-bar progress-bar-success"
             role="progressbar"
             aria-valuenow="{{ $result['popularity'] }}"
             aria-valuemin="0"
             aria-valuemax="100"
             style="width: {{ $result['popularity'] }}%">
            {{ $result['name'] }}
        </div>
        <span class="pull-right">{{ round($result['popularity'], 2) }}%</span>

    </div>
    @endforeach
    @endif

    <div class="text-right">
                <span class="label label-default">
                    @if( isset($widget['stream']->last_pull) )
                        Updated at: {{ $widget['stream']->last_pull }}
                    @else
                        Waiting for data ...
                    @endif
                </span>
    </div>