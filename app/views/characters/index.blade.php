@extends('layout')
@section('content')


<div class="container">

    <h2>Characters</h2>

    @if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif

    <div class="row control-group">

        <div class="col-lg-6">
            <a class="btn btn-default btn-warning" href="{{ URL::to('characters/update') }}">
                <span class="glyphicon glyphicon-refresh"></span>
                Update
            </a>
        </div>

        <div class="col-lg-6">
            <form class="input-group custom-search-form" action="{{ URL::to('characters') }}">
                <input type="text" class="form-control" value="{{ Input::get('query') }}" name="query"
                       placeholder="Search" autocomplete="off" autofocus="autofocus">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
                </span>
            </form>
        </div>
    </div>
    <br/>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td>Id</td>
            <td class="col-sm-4">Image</td>
            <td>Name</td>
            <td>Description</td>
            <td class="col-sm-2">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($characters as $key => $value)
        <tr>
            <td>{{ $value->id }}</a></td>
            <td>
                <a href="{{ $value->thumbnail['path'] }}.{{ $value->thumbnail['extension'] }}">
                    <img width="100"
                         src="{{ $value->thumbnail['path'] }}.{{ $value->thumbnail['extension'] }}"/>
                </a>
            </td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->description }}</td>

            <td>
                <a class="btn btn-sm btn-success" href="{{ URL::to('characters/' . $value->id) }}">Details</a>
                <a class="btn btn-sm btn-info" href="{{ URL::to('characters/' . $value->id . '/edit') }}">Edit</a>
            </td>
        </tr>
        @endforeach

        </tbody>
    </table>
    <div class="text-center">
        {{ $characters->appends(array('query'=>Input::get('query') ))->links() }}
    </div>
</div>
@stop