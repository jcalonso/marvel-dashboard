@extends('layout')
@section('content')

<div class="container streams-create">
    <h2>New stream</h2>

    <!-- if there are creation errors, they will show here -->
    {{ HTML::ul($errors->all()) }}

    <div class="row">

        <div class="col-lg-12">
            {{ Form::open(array('url' => 'streams')) }}

            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', Input::old('name'), array('class' => 'form-control', 'autofocus')) }}
            </div>

            <div class="form-group">
                {{ Form::label('streamType', 'Type') }}
                {{ Form::select('streamType', $streamTypes, Input::old('streamType'), array('class' => 'form-control')) }}
            </div>

            <div class="form-group" id="marvel-characters">
                <input type="hidden" name="selectedCharacters" id="selectedCharacters" value="">
                <input  class="typeahead form-control" type="text" placeholder="Select Marvels characters">
            </div>

            {{ Form::submit('Create stream', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}

        </div>

        <div class="col-lg-9"></div>

    </div>


</div>
@stop