@extends('layout')
@section('content')


<div class="container">

    <h2>Streams</h2>

    @if (Session::has('message'))
    <div class="alert alert-success">{{ Session::get('message') }}</div>
    @endif

    <div class="row control-group">
        <div class="col-md-6">
            <a class="btn btn-sm btn-primary" href="{{ URL::to('streams/create') }}">Add</a>
        </div>

        <div class="col-md-6">
            <form class="input-group custom-search-form" action="{{ URL::to('characters') }}">
                <input type="text" class="form-control" value="{{ Input::get('query') }}" name="query"
                       placeholder="Search" autocomplete="off" autofocus="autofocus">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button">
                    <span class="glyphicon glyphicon-search"></span>
                </button>
                </span>
            </form>
        </div>
    </div>
    <br/>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td class="col-md-1">Hash</td>
            <td>Name</td>
            <td>CSDL</td>
            <td>Status</td>
            <td class="col-md-2">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($streams as $key => $value)
        <tr>
            <td>
                <button type="button" class="btn btn-default hash-popover"
                        data-toggle="popover"
                        data-placement="top"
                        data-content="{{ $value->hash }}">
                    {{ str_limit($value->hash, $limit = 5, $end = '...') }}
                </button>
                </a>
            </td>
            <td>{{ $value->name }}</a></td>
            <td>{{ $value->csdl }}</td>
            <td>{{ $value->status }}</td>

            <td>

                {{ Form::open(array('url' => 'streams/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', array('class' => 'btn btn-sm btn-danger')) }}
                {{ Form::close() }}

                @if( !empty( $value->subscriptionId ) )
                {{ Form::open(array('url' => 'streams/unsubscribe/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::submit('Stop', array('class' => 'btn btn-sm btn-warning')) }}
                {{ Form::close() }}

                @else
                {{ Form::open(array('url' => 'streams/subscribe/' . $value->id, 'class' => 'pull-right')) }}
                {{ Form::submit('Start', array('class' => 'btn btn-sm btn-success')) }}
                {{ Form::close() }}
                @endif


            </td>
        </tr>
        @endforeach

        </tbody>
    </table>
    <div class="text-center">
        {{ $streams->appends(array('query'=>Input::get('query') ))->links() }}
    </div>
</div>
@stop