@foreach($characters as $character)
    tag "{{ $character['id'] }}"     { interaction.content contains "{{  $character['name'] }}" }
@endforeach

return {
    interaction.content any "{{ $search }}"
}