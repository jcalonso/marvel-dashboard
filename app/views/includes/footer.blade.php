<div class="container">
    <hr>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-8">
                <h6 class="muted pull-left"><a href="http://jcalonso.com">Juan Carlos Alonso</a></h6>
            </div>
            <div class="col-md-4 text-right">
                <a href="http://twitter.com/jcalonso86"><img src="/images/twitter.png" width="32"/></a>
            </div>
        </div>
    </div>
</div>

<!--Le Javascript-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="/tagsinput.js"></script>
<script src="//timschlechter.github.io/bootstrap-tagsinput/examples/bower_components/typeahead.js/dist/typeahead.min.js"></script>
<script src="/main.js"></script>
