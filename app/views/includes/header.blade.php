<div class="navbar-wrapper">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                    <img width="75" src="/images/datasift-logo.png">
                    Endless Marvels</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is( 'dashboard') ? 'active' : '' }}">
                        <a href="{{ URL::to('dashboard')}}">
                            <span class="glyphicon glyphicon-home"></span>&nbsp;Dashboard
                        </a>
                    </li>
                    <li class="{{ Request::is( 'streams*') ? 'active' : '' }}">
                        <a href="{{ URL::to('streams')}}">
                            <span class="glyphicon glyphicon-flash"></span>&nbsp;Streams
                        </a>
                    </li>
                    <li class="{{ Request::is( 'characters*') ? 'active' : '' }}">
                        <a href="{{ URL::to('characters')}}">
                            <span class="glyphicon glyphicon-user"></span>&nbsp;Characters
                        </a>
                    </li>
                    <li class="{{ Request::is( 'settings*') ? 'active' : '' }}">
                        <a href="{{ URL::to('settings/logs')}}">
                            <span class="glyphicon glyphicon-cog"></span>&nbsp;Settings
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
    </div>
</div>