<?php

class StreamsController extends \BaseController
{
    protected $validatorRules = array(
            'name'               => 'required|min:3',
            'selectedCharacters' => 'required'

    );

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if ( Input::get( 'query' ) ) {
            // Get results based on the search query
            $streams = Stream::where( 'name', 'LIKE', '%' . Input::get( 'query' ) . '%' )
                             ->where( 'hash', Input::get( 'query' ) )
                             ->paginate( 30 );
        } else {
            // Get all streams paginated
            $streams = Stream::paginate( 30 );
        }

        return View::make( 'streams.index' )
                   ->with( 'streams', $streams );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $streamTypes = StreamType::all()
                                 ->lists( 'name', 'id' );
        return View::make( 'streams.create' )
                   ->with( 'streamTypes', $streamTypes );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validator = Validator::make( Input::all(), $this->validatorRules );


        if ( $validator->fails() ) {
            return Redirect::to( 'streams/create' )
                           ->withErrors( $validator )
                           ->withInput( Input::all() );
        } else {

            $cleanCharactersIds = $this->cleanCharacterIds( Input::get( 'selectedCharacters' ) );
            $renderedCsdlQuery = $this->renderCsdlQuery( Input::get( 'streamType' ),
                                                         $cleanCharactersIds );

            // Save to storage a reference for later processing
            $stream = new Stream;
            $stream->name = Input::get( 'name' );
            $stream->hash = 'waiting';
            $stream->stream_type_id = Input::get( 'streamType' );
            $stream->csdl = $renderedCsdlQuery;
            $stream->characters = $cleanCharactersIds;
            $stream->status = 'queued';
            $stream->save();

            // Enqueue the stream creating and subscription
            Queue::push( 'CreateStream', $stream->id );

            Session::flash( 'message', 'Stream created, soon it will start fetching interactions from DataSift' );
            return Redirect::to( 'streams' );
        }
    }

    private function renderCsdlQuery( $streamType, $charactersIds )
    {
        $characters = Character::whereIn( 'id', $charactersIds )->get();

        // Clean characters names
        $cleanCharacterNames = [ ];

        foreach ( $characters as $character ) {
            $name = trim( preg_replace( "/\\((.*?)\\)/u", "", $character->name ) );
            $cleanCharacterNames[$name] = [ 'id' => $character->id, 'name' => $name ];
        }

        $csdlTemplate = StreamType::find( $streamType )->csdl_template;

        // Render the CSDL template for this streamtype using blade
        $csdlRendered = View::make( $csdlTemplate )
                            ->with( 'search', implode( ',', array_keys( $cleanCharacterNames ) ) )
                            ->with( 'characters', $cleanCharacterNames )
                            ->render();

        return $csdlRendered;
    }

    public function cleanCharacterIds( $rawIds )
    {
        // Get the characters names from the ids
        $charactersIds = explode( ',', $rawIds );
        $charactersIds = array_map( function ( $value ) {
            return (int)$value;
        }, $charactersIds );

        return $charactersIds;
    }

    /**
     * Register a new subscription for an existing stream
     *
     * @param  int $id
     * @return Response
     */
    public function subscribe( $id )
    {
        // Save to storage a reference for later processing
        $stream = Stream::find( $id );
        $stream->status = 'queued';
        $stream->save();

        // Enqueue the stream creating and subscription
        Queue::push( 'CreateStream', $stream->id );

        Session::flash( 'message', 'New subscription created, soon it will start fetching interactions from DataSift' );
        return Redirect::to( 'streams' );
    }

    public function unsubscribe($id)
    {
        // Enqueue stream deletion
        Queue::push( 'DeleteSubscription', $id );

        // redirect
        Session::flash( 'message', 'Stream subscription deleted' );
        return Redirect::to( 'streams' );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update( $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy( $id )
    {
        // Enqueue stream deletion
        Queue::push( 'DeleteSubscription', $id );

        // Delete stream
        Stream::find($id)->delete();

        // redirect
        Session::flash( 'message', 'Stream and subscription deleted' );
        return Redirect::to( 'streams' );
    }

}