<?php

class DashboardController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $streams = Stream::all();

        $widgets = [ ];
        foreach ( $streams as $stream ) {

            $characters = Character::whereIn( 'id', $stream->characters )
                                   ->groupBy( 'id' )
                                   ->get( [ 'id', 'name', 'thumbnail' ] );

            $streamType = StreamType::find( $stream->stream_type_id );

            $widgets[] = [ 'stream' => $stream, 'characters' => $characters, 'streamType' => $streamType ];
        }

        return View::make( 'dashboard.index' )
                   ->with( 'widgets', $widgets );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show( $id )
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit( $id )
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update( $id )
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy( $id )
    {
        //
    }


}
