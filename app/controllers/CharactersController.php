<?php

class CharactersController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if ( Input::get( 'query' ) ) {
            // Get results based on the search query
            $characters = Character::where( 'name', 'LIKE', '%' . Input::get( 'query' ) . '%' )
                                   ->paginate( 30 );
        } else {
            // Get all characters paginated
            $characters = Character::paginate( 30 );
        }

        return View::make( 'characters.index' )
                   ->with( 'characters', $characters );
    }

    public function json()
    {
        $characters = Character::select('id', 'name')->get();
        return $characters;
    }

    public function refresh()
    {
        Queue::push( 'UpdateCharacters' );

        Session::flash( 'message', 'Updating characters, hold on, this may take a minute or two.' );
        return Redirect::to( 'characters' );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show( $id )
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit( $id )
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update( $id )
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy( $id )
    {
        //
    }


}
