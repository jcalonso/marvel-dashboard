<?php

class StreamTypesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table( 'stream_type' )->delete();
        StreamType::create(
                [
                        'name'            => 'Which character is more popular?',
                        'csdl_template'   => 'csdl.popularcharacters',
                        'widget_template' => 'widgets.popularity',
                ] );
        StreamType::create(
                [
                        'name'            => 'Which characters are liked or not?',
                        'csdl_template'   => 'csdl.popularcharacters',
                        'widget_template' => 'widgets.likability',
                ] );
        StreamType::create(
                [
                        'name'            => 'Which characters are more popular by gender?',
                        'csdl_template'   => 'csdl.popularcharacters',
                        'widget_template' => 'widgets.likabilitybygenre',
                ] );
    }

}