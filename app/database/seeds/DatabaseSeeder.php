<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('StreamTypesTableSeeder');

        $this->command->info('StreamType collection seeded!');
	}

}
