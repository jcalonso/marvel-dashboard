<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get( '/', array( 'uses' => 'DashboardController@index' ) );
Route::get( '/dashboard', array( 'uses' => 'DashboardController@index' ) );
Route::resource( 'streams', 'StreamsController' );
Route::post( 'streams/unsubscribe/{id}', 'StreamsController@unsubscribe' );
Route::post( 'streams/subscribe/{id}', 'StreamsController@subscribe' );
Route::get( 'characters/update', array( 'uses' => 'CharactersController@refresh' ) );
Route::get( 'characters', array( 'uses' => 'CharactersController@index' ) );
Route::get( 'characters/json', array( 'uses' => 'CharactersController@json' ) );

Route::get( 'settings/logs', ['uses' => 'SettingsController@index'] );